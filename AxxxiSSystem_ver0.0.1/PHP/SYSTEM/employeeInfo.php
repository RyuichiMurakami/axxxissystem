<?php session_start(); ?>
<?php
  header("Cache-Control: no-cache");
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png"><!-- スマホとかのタッチアイコン？ -->
  <link rel="icon" type="image/png" href="../assets/img/favicon.png"><!-- PCでタブの横にでてくるアレ -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="robots" content="noindex" /><!-- クローラーに無視してもらうようにお願いする -->
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' /><!-- レスポンシブ対応 -->

  <!-- JQuery・チャート・カラーパレット読み込み -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script><!--CDN経由でJQuery読み込み（ver3.4.1）-->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
  <script type="text/javascript" src="https://github.com/nagix/chartjs-plugin-colorschemes/releases/download/v0.2.0/chartjs-plugin-colorschemes.min.js"></script>

  <title>Axxxis System</title>

  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Files -->
  <link href="../../BOOTSTRAP/CSS/bootstrap.min.css" rel="stylesheet" />
  <link href="../../CSS/mainStyle.css" rel="stylesheet" />
</head>

<?php
  /*-------------------------注意！！-------------------------
    ｜このプログラムはPHP5.3.6以上でUTF-8を使う場合の接続方法です｜
    ---------------------------------------------------------
  */
  //session_start();
  include "../PHPLIB/axs_UICustom.php";  //UIカスタムライブラリーを読み込み



  /*--------------------------その他のPHPファイル--------------------------
    ｜"PHP/regist.php"     //登録・削除等のボタンを押した後に表示するファイル｜
    ｜"PHP/LibReadMe.php"  //ライブラリーに関する補足事項等を記述したファイル｜
    ----------------------------------------------------------------------
  */

  /*---------------------アクセスURL---------------------
    ｜http://localhost/AxxxiSSystem/PHP/SYSTEM/employeeInfo.php："employeeInfo.php"の場所              ｜
    ｜http://localhost/dashboard/:XAMPP：ダッシュボード  ｜
    ｜http://localhost/phpmyadmin：phpMyAdmin          ｜
    ----------------------------------------------------
  */

  try{


    if(!isset($_SESSION['ID'])) {$_SESSION['ID'] = $_REQUEST['loginID']; }
    if(!isset($_SESSION['PW'])) {$_SESSION['PW'] = $_REQUEST['loginPW']; }

    //事前準備
    $dbName = DBNAME_LIST[0];//接続するDB名をここで定義
    $DBC = new DB_Class();//新しいDBクラスを定義
    db_Init($DBC,DB_HOST,DB_USER,DB_PASSWORD,DB_PORT,$dbName);//DB情報を変更
    $NewPDO = pdo_Make($DBC);//DBに接続する為のPDOを生成
    $tableName = TABLENAME_LIST_ST[0];//masterinfo。接続するテーブル名をここで定義。これをやっておかないと、以下の!isset($_REQUEST['dbQuery'])にいれると２回目以降に無定義状態になるらしい。

    //データベースへアクセス

    $sql = "SELECT * FROM ".$tableName." WHERE id = "."'".$_SESSION['ID']."'";
    $statement = $NewPDO->query($sql);//全ての情報を保存
    $row = array();//配列　"row"を定義
    $row = $statement->fetchAll(PDO::FETCH_ASSOC);//データベースを配列情報に変換して、入れる。PDO::FETCH_ASSOC・・・フィールド名で添字を付けた配列を返す
    //var_dump($row);

    $_SESSION['NAME'] = $_SESSION['NAMERUBY'] = $_SESSION['AT'] = $_SESSION['DM'] = "";


    if($_SESSION['NAME'] != $row[0]['name']) { $_SESSION['NAME'] = $row[0]['name']; }
    if($_SESSION['NAMERUBY'] != $row[0]['nameRuby']) { $_SESSION['NAMERUBY'] = $row[0]['nameRuby']; }
    if($_SESSION['AT'] != $row[0]['accessType']) { $_SESSION['AT'] = $row[0]['accessType']; }
    if($_SESSION['DM'] != $row[0]['department']) { $_SESSION['DM'] = $row[0]['department']; }

    $bool = false;

    //画像アップロード処理
    $bool2 = false;
    $message = "";

    if(isset($_REQUEST['profileImageUpdate'])){
      if($_REQUEST['profileImageUpdate'] === "更新"){
        profileIMGUpload($_SESSION['ID'],$_FILES['upfile']['tmp_name'],$_FILES['upfile']['name'],$bool2,$message,700,1000,80);
      }
    }
    //画像アップロード終了

    //プロフィール更新処理
    if(isset($_REQUEST['profileUpdate'])){
      if($_REQUEST['profileUpdate'] === "プロフィールを更新する"){
          $bool = true;

          /*
          $dataList = [
            'no' => $_REQUEST['no'],
            'id' => $_REQUEST['id'],
            'pw' => $_REQUEST['pw'],
            'accessType' => $_REQUEST['accessType'],
            'name' => $_REQUEST['name'],
            'nameRuby' => $_REQUEST['nameRuby'],
            'sex' => $_REQUEST['sex'],
            'age' => $_REQUEST['age'],
            'birthday' => $_REQUEST['birthday'],
            'zipCode' => $_REQUEST['zipCode'],
            'address' => $_REQUEST['address'],
            'telNumberHome' => $_REQUEST['telNumberHome'],
            'telNumberMobile' => $_REQUEST['telNumberMobile'],
            'telNumberEmergency' => $_REQUEST['telNumberEmergency'],
            'email' => $_REQUEST['email'],
            'selfAppeal' => $_REQUEST['selfAppeal'],
            'qualification' => $_REQUEST['qualification']
          ];
          */

          unset($_REQUEST['profileUpdate']);

          $_SESSION['ID'] = $_REQUEST['id'];
          $_SESSION['PW'] = $_REQUEST['pw'];

          //profileUpdateExe($dataList, $tableName,$NewPDO);//UPDATE実行
          DB_PROCESS($_REQUEST, $dbName, $tableName, $NewPDO,"UPDATE_MODE","profile","");//UPDATE実行

          $sql = "SELECT * FROM ".$tableName." WHERE id = "."'".$_SESSION['ID']."'";
          $statement = $NewPDO->query($sql);//全ての情報を保存
          $rowRow = array();
          $rowRow = $statement->fetchAll(PDO::FETCH_ASSOC);//データベースを配列情報に変換して、入れる。PDO::FETCH_ASSOC・・・フィールド名で添字を付けた配列を返す

          //データベースへアクセス
          $sql = "SELECT * FROM ".$tableName." WHERE id = "."'".$_SESSION['ID']."'";
          $statement = $NewPDO->query($sql);//全ての情報を保存
          $row = array();//配列　"row"を定義
          $row = $statement->fetchAll(PDO::FETCH_ASSOC);//データベースを配列情報に変換して、入れる。PDO::FETCH_ASSOC・・・フィールド名で添字を付けた配列を返す

          if(!isset($_SESSION['NAME']) || $_SESSION['NAME'] != $row[0]['name']) { $_SESSION['NAME'] = $row[0]['name']; }
          if(!isset($_SESSION['NAMERUBY']) || $_SESSION['NAMERUBY'] != $row[0]['nameRuby']) { $_SESSION['NAMERUBY'] = $row[0]['nameRuby']; }
          if(!isset($_SESSION['AT']) || $_SESSION['AT'] != $row[0]['accessType']) { $_SESSION['AT'] = $row[0]['accessType']; }
          if(!isset($_SESSION['DM']) || $_SESSION['DM'] != $row[0]['department']) { $_SESSION['DM'] = $row[0]['department']; }

      }
    }
    //プロフィール更新処理終了

  }catch(PDOException $e){
    header('Content-Type: text/plain; charset=UTF-8', true, 500);
    exit($e->getMessage()); //エラーの内容を吐き出す
  }
?>

<script>
//page topボタン
$(function(){

  //事前準備
  var userAgent = window.navigator.userAgent.toLowerCase();//使用しているブラウザを調べる
  var interF = "";
  if(userAgent.indexOf('msie') != -1 || userAgent.indexOf('trident') != -1) { interF = "IE"; }
  else if(userAgent.indexOf('edge') != -1) { interF = "Edge"; }
  else if(userAgent.indexOf('chrome') != -1) { interF = "Chrome"; }
  else if(userAgent.indexOf('safari') != -1) { interF = "Safari"; }
  else if(userAgent.indexOf('firefox') != -1) { interF = "firefox"; }
  else if(userAgent.indexOf('opera') != -1) { interF = "opera"; }
  var y = 0;
  var targetElement = document.getElementById( "temp" ) ;
  var clientRect = targetElement.getBoundingClientRect() ;
  var max_y = clientRect.top ;// 画面内の位置
  var pagetop=$('#target');
  pagetop.hide();

  setInterval(function(){

    if(interF == "Safari"){
      var y = window.pageYOffset;
      //alert(y);
      if(y > 300) { pagetop.fadeIn(); }
      else { pagetop.fadeOut(); }
    }
    else{
      targetElement = document.getElementById( "temp" ) ;
      clientRect = targetElement.getBoundingClientRect() ;
      y = clientRect.top ;// 画面内の位置
      var py = max_y - clientRect.top ;// ページ内の位置
      //console.log(py);
      if(py > 300) { pagetop.fadeIn(); /*$('.footer' + '.fixed-bottom').css('display','');*/ }
      else { pagetop.fadeOut(); }
    }
  },1000);
});
</script>

<body class="user-profile">



<div class="page-wrapper chiller-theme toggled">
  <a id="show-sidebar" class="btn btn-sm btn-dark" href="#"><i class="fas fa-bars"></i></a>

  <!--sidebar-->
  <?php sidebarMake("プロフィール",$_SESSION); ?>

  <!-- sidebar-wrapper  -->
  <main class="page-content">
    <div class="container-fluid">
      <div class="wrapper">
        <div class="main-panel" id="main-panel">

          <!-- End Navbar -->
          <div class="panel-header panel-header-sm"></div>

          <div class="content">
            <div class="row">

              <div class="col-md-4">
                <div class="card card-user">
                  <div class="card-body">
                      <p class="description text-center">
                        <div class="author"><a href="#" class="e_img"><img class="avatar border-gray img_size" src="<?php echo '../../EMPLOYEE_IMG/'.$_SESSION['ID'].'.jpg'; ?>" alt="..."></a></div>
                        <center><h5 class="title"><?php echo BR_WIN.$row[0]['name'].BR_WIN; ?></h5></center>
                      </p>
                  </div>

                  <!-- ProfileIMG UpdateForm -->
                  <form action="./employeeInfo.php" method="post" enctype="multipart/form-data">
                    <div class="row">
                      <div class="col-md-12" style="overflow-x:auto; text-align:center;">

                        <div class="form-group" style="display:inline-block;">
                          <div>
                            <label class="test" for="file_upload">プロフィール画像選択
                              <input type="file"  name="upfile" id="file" style="position:relative; opacity:1.0; font-size:0.5vw;" onchange="$('#fake_text_box').val($(this).val())">
                              <input type="text" id="file_upload" value="ファイル選択" onClick="$('#file').click();">
                            </label>
                          </div>
                          <div>
                            <br /><input type="text" id="fake_text_box" value="" style="width:100%;" readonly>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12"><center>
                        <input type="submit" class="btn btn-blueVer rounded-pill" name="profileImageUpdate" value="更新">
                        <?php
                          if($bool2 == true) { echo '<br /><center><p>'.$message.'</p></center>'; }
                        ?>
                      </center></div>

                    </div>
                    <div id="temp"></div>
                  </form>

                </div>
              </div>

              <div class="col-md-8">
                <div class="card">
                  <div class="card-header"><h5 class="title">プロフィール</h5></div>
                  <div class="card-body">
                    <form action="./employeeInfo.php" name="profileForm">
                      <div class="row">
                        <?php profileMake("id", "","6", "", "ID", "sBGDR", $row[0]['id'],"","",""); ?>
                        <?php profileMake("pw", "","6", "", "パスワード", "AdynDBwq459", $row[0]['pw'],"","",""); ?>
                        <?php profileMakePulldownMode("accessType", "", "6", "", "役職（アクセス権限）", PULLDOWNLIST_ACCESSTYPE, $row[0]['accessType'], ""); ?>
                        <?php profileMakePulldownMode("department", "", "6", "", "所属部署", PULLDOWNLIST_DEPARTMENT, $row[0]['department'], ""); ?>
                      </div>

                      <div class="row">
                        <?php profileMake("name", "","12", "", "氏名", "中島勇太", $row[0]['name'],"","",""); ?>
                        <?php profileMake("nameRuby", "","8", "", "フリガナ", "ナカジマユウタ", $row[0]['nameRuby'],"","",""); ?>
                        <?php profileMake("sex", "","2", "", "性別", "男", $row[0]['sex'],"","",""); ?>
                        <?php profileMake("age", "","2", "", "年齢", "8", $row[0]['age'],"","",""); ?>
                        <?php profileMake("birthday", "","12", "", "生年月日", "1991-08-15", $row[0]['birthday'],"","",""); ?>
                      </div>

                      <div class="row">
                        <?php profileMake("zipCode", "","4", "", "郵便番号", "XXX-XXXX", $row[0]['zipCode'],"","",""); ?>
                        <?php profileMake("address", "","8", "", "住所", "○○県△△市××町X-X-XX　□□マンション　XXX号室", $row[0]['address'],"","",""); ?>
                      </div>
                      <div class="row">
                        <?php profileMake("telNumberHome", "","4", "", "電話番号（自宅）", "090-XXXX-XXXX", $row[0]['telNumberHome'],"","",""); ?>
                        <?php profileMake("telNumberMobile", "","4", "", "電話番号（携帯）", "090-XXXX-XXXX", $row[0]['telNumberMobile'],"","",""); ?>
                        <?php profileMake("telNumberEmergency", "","4", "", "電話番号（緊急）", "090-XXXX-XXXX", $row[0]['telNumberEmergency'],"","",""); ?>
                        <?php profileMake("email", "","12", "", "Email", "○○.ne.jp", $row[0]['email'],"","",""); ?>
                      </div>
                      <div class="row">
                        <?php profileMake("alongLine", "","12", "", "最寄駅", "最寄駅を自由に記入して下さい。", $row[0]['alongLine'],"FREE","4","80"); ?>
                        <?php profileMake("selfAppeal", "","12", "", "自己アピール", "自己紹介文を自由に記入して下さい。", $row[0]['selfAppeal'],"FREE","4","80"); ?>
                        <?php profileMake("qualification", "","12", "", "資格／免許", "資格／免許を記入して下さい。", $row[0]['qualification'],"FREE","4","80"); ?>
                      </div>
                      <input name="no" type="hidden" value="<?php echo $row[0]['no']; ?>">
                      <br />
                      <center><input type="submit" class = "btn btn-blueVer rounded-pill" name="profileUpdate" value="プロフィールを更新する"></center>
                      <?php
                        if($bool == true) { echo '<br /><center><p>プロフィールの更新が完了しました。</p></center>'; }
                      ?>
                    </form>
                  </div>
                </div>
              </div>

            </div>
          </div>

          <div id="temp" style="height:20vh;"></div>
          <footer class="footer fixed-bottom" id = "target" style="display:none;">
            <div class=" container-fluid ">
              <nav style="float:right;">
                <a href="#main-panel" style="float:right;"><i class="now-ui-icons arrows-1_minimal-up btn btn-orangeVer rounded-pill"></i></a>
              </nav>
            </div>
          </footer>

        </div>
      </div>

    </div>
  </main>
  <!-- page-content" -->
</div>
<!-- page-wrapper -->


  <!--   Core JS Files   -->
  <script src="../../BOOTSTRAP/JS/jquery.min.js"></script>
  <script src="../../BOOTSTRAP/JS/popper.min.js"></script>
  <script src="../../BOOTSTRAP/JS/bootstrap.min.js"></script>
  <script src="../../BOOTSTRAP/JS/bootstrap-notify.js"></script>
  <script src="../../BOOTSTRAP/JS/sidebar.js"></script>

</body>
</html>
