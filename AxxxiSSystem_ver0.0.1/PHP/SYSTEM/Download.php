<?php
  $isContain=false;//ホワイトリストに含まれているか？
  /*
  ホワイトリストとなるディレクトリを指定
  (このディレクトリ以外のファイルはダウンロードさせない)
  */

  $path=$_GET['file'];//一つ一つのファイルを表示させてゆく。
  $fPartName = explode("_",$path);//ファイル名を分割（./ITST/123456_OFFICER_EX_payslip_2020_06.pdf　⇒　[0] = ./ITST/123456, [1] = OFFICER, ・・・）
  $fold = './'.$fPartName[2];
  var_dump($fold);
  if(!file_exists($fold)) { die('ダウンロードしようとしたファイルを格納しているフォルダーそのものが存在しません。'); }
  $fold = $fold.'/';
  $dir=opendir($fold);
  $i = 0;

  //ディレクトリの走査
  while($file=readdir($dir)){
    if(is_file($fold .$file)){
      $path=$fold.$file;//対象となったパスを保存しておく
      //クエリで指定されたファイルがディレクトリにあればOK
      if($_GET['file']===$file){
        //あったのでフラグをtrue
        $isContain=true;
        //抜ける
        break;
      }
      $i++; var_dump($i);
      if($i > 100) { break; }
    }
  }
  closedir($dir);//dirを閉じる
  //クエリで渡されたファイル名が不正だったら終了
  if(!$isContain){
    var_dump($_GET['file']);
    die('不正なパスが指定されました。');
  }
  //不正でなければダウンロードする
  $filesize = filesize($path);
  header('Content-Type:application/octet-stream');
  header('Content-Length:'.$filesize);
  header('Content-Disposition:attachment;filename='.$file);
  readfile($path);
