<?php session_start(); ?>
<?php
  header("Cache-Control:no-cache,no-store,must-revalidate,max-age=0");
  header("Cache-Control:pre-check=0","post-check=0",false);
  header("Pragma:no-cache");
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png"><!-- スマホとかのタッチアイコン？ -->
  <link rel="icon" type="image/png" href="../assets/img/favicon.png"><!-- PCでタブの横にでてくるアレ -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="robots" content="noindex" /><!-- クローラーに無視してもらうようにお願いする -->
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' /><!-- レスポンシブ対応 -->

  <!-- JQuery・チャート・カラーパレット読み込み -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script><!--CDN経由でJQuery読み込み（ver3.4.1）-->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
  <script type="text/javascript" src="https://github.com/nagix/chartjs-plugin-colorschemes/releases/download/v0.2.0/chartjs-plugin-colorschemes.min.js"></script>

  <title>Axxxis System</title>

  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Files -->
  <link href="../../BOOTSTRAP/CSS/bootstrap.min.css" rel="stylesheet" />
  <link href="../../CSS/mainStyle.css" rel="stylesheet" />
</head>

<?php
  /*-------------------------注意！！-------------------------
    ｜このプログラムはPHP5.3.6以上でUTF-8を使う場合の接続方法です｜
    ---------------------------------------------------------
  */
  //session_start();
  include "../PHPLIB/axs_UICustom.php";  //UIカスタムライブラリーを読み込み



  /*--------------------------その他のPHPファイル--------------------------
    ｜"PHP/regist.php"     //登録・削除等のボタンを押した後に表示するファイル｜
    ｜"PHP/LibReadMe.php"  //ライブラリーに関する補足事項等を記述したファイル｜
    ----------------------------------------------------------------------
  */

  /*---------------------アクセスURL---------------------
    ｜http://localhost/AxxxiSSystem/PHP/SYSTEM/employeeInfo.php："employeeInfo.php"の場所              ｜
    ｜http://localhost/dashboard/:XAMPP：ダッシュボード  ｜
    ｜http://localhost/phpmyadmin：phpMyAdmin          ｜
    ----------------------------------------------------
  */

  try{
    $bool = false;

    //プロフィール更新処理
    if(isset($_REQUEST['payslipUpload'])){
      if($_REQUEST['payslipUpload'] === "給与明細をアップロード"){
          $bool = true;
          $message = "";
          $info=pathinfo($_FILES['upfile']['name']);
        	$ext = ['pdf'];//許す拡張子のリスト
        	$result = $_FILES['upfile']['error'];
          $fName = explode("_",$info['filename']);
          $errorCode = filenameCheck($fName);
          if($errorCode != "") { $message = $errorCode; }
          else{
            //エラーはあるか(0:UPLOAD_ERR_OKなら正常)
          	if($result !==UPLOAD_ERR_OK){ $message ='アップロードエラーが発生しました。'; /*アップロードはエラーはないか？*/ }
            else if(!in_array(strtolower($info['extension']),$ext)){ $message='PDF以外のファイルはアップロードできません。'; /*拡張子は許されたものか？*/ }
          else if(!move_uploaded_file($_FILES['upfile']['tmp_name'], $fName[2].'/'.basename($_FILES['upfile']['name']) )){ $message='書き込みに失敗しました。';/*書き込みに成功したか？*/ }
          if($message != ""){ die('error:'.$message);/*エラーがあったら出力して終了*/ }
            $message = "給与明細のアップロードが完了しました。";
          }

      }
    }
    //プロフィール更新処理終了

  }catch(PDOException $e){
    header('Content-Type: text/plain; charset=UTF-8', true, 500);
    exit($e->getMessage()); //エラーの内容を吐き出す
  }
?>

<script>
//page topボタン
$(function(){

  //事前準備
  var userAgent = window.navigator.userAgent.toLowerCase();//使用しているブラウザを調べる
  var interF = "";
  if(userAgent.indexOf('msie') != -1 || userAgent.indexOf('trident') != -1) { interF = "IE"; }
  else if(userAgent.indexOf('edge') != -1) { interF = "Edge"; }
  else if(userAgent.indexOf('chrome') != -1) { interF = "Chrome"; }
  else if(userAgent.indexOf('safari') != -1) { interF = "Safari"; }
  else if(userAgent.indexOf('firefox') != -1) { interF = "firefox"; }
  else if(userAgent.indexOf('opera') != -1) { interF = "opera"; }
  var y = 0;
  var targetElement = document.getElementById( "temp" ) ;
  var clientRect = targetElement.getBoundingClientRect() ;
  var max_y = clientRect.top ;// 画面内の位置
  var pagetop=$('#target');
  pagetop.hide();

  setInterval(function(){

    if(interF == "Safari"){
      var y = window.pageYOffset;
      //alert(y);
      if(y > 300) { pagetop.fadeIn(); }
      else { pagetop.fadeOut(); }
    }
    else{
      targetElement = document.getElementById( "temp" ) ;
      clientRect = targetElement.getBoundingClientRect() ;
      y = clientRect.top ;// 画面内の位置
      var py = max_y - clientRect.top ;// ページ内の位置
      //console.log(py);
      if(py > 300) { pagetop.fadeIn(); /*$('.footer' + '.fixed-bottom').css('display','');*/ }
      else { pagetop.fadeOut(); }
    }
  },1000);

  //$(".sidebar-dropdown3 > a").removeClass("active");
  //$(".sidebar-dropdown3 > a").next(".sidebar-submenu3").slideDown(200);

});
</script>

<body class="user-profile">



<div class="page-wrapper chiller-theme toggled">
  <a id="show-sidebar" class="btn btn-sm btn-dark" href="#"><i class="fas fa-bars"></i></a>

  <!--sidebar-->
  <?php sidebarMake("給与明細Upload",$_SESSION); ?>

  <!-- sidebar-wrapper  -->
  <main class="page-content">
    <div class="container-fluid">
      <div class="wrapper">
        <div class="main-panel" id="main-panel">

          <!-- End Navbar -->
          <div class="panel-header panel-header-sm"></div>

          <div class="content">
            <div class="row">

              <div class="col-md-12">
                <div class="card">
                  <div class="card-header"><h5 class="title">給与明細Upload</h5></div>
                  <div class="card-body">

                    <!-- 給与明細アップロード -->
                    <form action="./payslipUpload.php" method="post" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-md-12"><p><center>以下の部分で給与明細のアップロードができます。アップロードするファイル名は<br />"社員ID_所属部署_役職_payslip_対象年度_対象月.pdf"<br />で統一して下さい。<br />
                        <br />例）社員IDが「123456」で所属部署が「ITP」、役職が「マネージャー」、2020年6月分の給与明細の場合<br /><br />123456_ITP_MGR_payslip_2020_06.pdf<br /><br /></center></p></div>
                        <div class="col-md-12"><center>アップロードファイル</center></div>
                        <div class="col-md-1"></div>
                        <div class="col-md-10" style="overflow-x:auto; text-align:center;">
                          <div class="form-group" style="display:inline-block;"><input type="file" name="upfile" style="position:relative; opacity:1.0;"></div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-12"><center>
                          <input type="submit" class="btn btn-blueVer rounded-pill" name="payslipUpload" value="給与明細をアップロード">
                          <?php
                            if($bool == true) { echo '<br /><center><p>'.$message.'</p></center>'; }
                          ?>
                        </center></div>

                      </div>
                      <div id="temp"></div>
                    </form>

                    <br /><br />

                    <div class="col-md-12">
                      <center><p>給与明細リスト</p></center>
                      <table border="1" style="width:100%; table-layout:fixed;">
                      <tr><th class="FN">ファイル名</th><th class="DF">サイズ</th><th class="DF">削除ボタン</th></tr>

                    <?php
                      // ディレクトリ(のみ)を取得
                      $dirs = glob('./*', GLOB_ONLYDIR) or die ('ディレクトリを開けませんでした。');;//同階層内のディレクトリーのみを取得
                      //var_dump($dirs);

                      for($i = 0; $i < count($dirs); $i++){
                        $dir2=@opendir($dirs[$i]) or die('フォルダが開けませんでした。');
                        $D_ROOT= $dirs[$i]."/";
                        //var_dump($D_ROOT);

                        $temp = "'"."削除してよろしいですか？"."'";

                        while($file=readdir($dir2)){
                          //ファイルか？（.や..やディレクトリが除外される）
                          //var_dump($file);
                          if(is_file($D_ROOT.$file)){
                            $path=$D_ROOT.$file;//一つ一つのファイルを表示させてゆく。
                            //var_dump($path);
                            //＄file:対象となるディレクトリのうち、現在開いているファイル名(path)
                            //$path:（./uploads/***.pdf

                            echo '<tr>'
                                  .'<td><a href="download.php?file='.$file.'">'.$file.'</a></td></td>'
                                  .'<td class="DF">'.round(filesize($path)/1024).'kb</td>'
                                  .'<td class="DF"><a href="unlink.php?file='.$file.'" onclick="return confirm('.$temp.')">削除</a></td></td>'
                                .'</tr>';
                          }
                        }
                        closedir($dir2);
                      }

                    ?>

                      </table>
                    </div>

                  </div>
                </div>
              </div>

            </div>
          </div>

          <div id="temp" style="height:20vh;"></div>
          <footer class="footer fixed-bottom" id = "target" style="display:none;">
            <div class=" container-fluid ">
              <nav style="float:right;">
                <a href="#main-panel" style="float:right;"><i class="now-ui-icons arrows-1_minimal-up btn btn-orangeVer rounded-pill"></i></a>
              </nav>
            </div>
          </footer>

        </div>
      </div>

    </div>
  </main>
  <!-- page-content" -->
</div>
<!-- page-wrapper -->


  <!--   Core JS Files   -->
  <script src="../../BOOTSTRAP/JS/jquery.min.js"></script>
  <script src="../../BOOTSTRAP/JS/popper.min.js"></script>
  <script src="../../BOOTSTRAP/JS/bootstrap.min.js"></script>
  <script src="../../BOOTSTRAP/JS/bootstrap-notify.js"></script>
  <script src="../../BOOTSTRAP/JS/sidebar.js"></script>

</body>
</html>
