<?php session_start(); ?>
<?php
  header("Cache-Control:no-cache,no-store,must-revalidate,max-age=0");
  header("Cache-Control:pre-check=0","post-check=0",false);
  header("Pragma:no-cache");
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png"><!-- スマホとかのタッチアイコン？ -->
  <link rel="icon" type="image/png" href="../assets/img/favicon.png"><!-- PCでタブの横にでてくるアレ -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="robots" content="noindex" /><!-- クローラーに無視してもらうようにお願いする -->
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' /><!-- レスポンシブ対応 -->

  <!-- JQuery・チャート・カラーパレット読み込み -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script><!--CDN経由でJQuery読み込み（ver3.4.1）-->
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
  <script type="text/javascript" src="https://github.com/nagix/chartjs-plugin-colorschemes/releases/download/v0.2.0/chartjs-plugin-colorschemes.min.js"></script>

  <!-- clndr.jsを使用可能にするためのライブラリの読み込み -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

  <title>Axxxis System</title>

  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- CSS Files -->
  <link href="../../BOOTSTRAP/CSS/bootstrap.min.css" rel="stylesheet" />
  <link href="../../CSS/mainStyle.css" rel="stylesheet" />
</head>

<?php
  /*-------------------------注意！！-------------------------
    ｜このプログラムはPHP5.3.6以上でUTF-8を使う場合の接続方法です｜
    ---------------------------------------------------------
  */
  //session_start();
  include "../PHPLIB/axs_UICustom.php";  //UIカスタムライブラリーを読み込み



  /*--------------------------その他のPHPファイル--------------------------
    ｜"PHP/regist.php"     //登録・削除等のボタンを押した後に表示するファイル｜
    ｜"PHP/LibReadMe.php"  //ライブラリーに関する補足事項等を記述したファイル｜
    ----------------------------------------------------------------------
  */

  /*---------------------アクセスURL---------------------
    ｜http://localhost/AxxxiSSystem/PHP/SYSTEM/employeeInfo.php："employeeInfo.php"の場所              ｜
    ｜http://localhost/dashboard/:XAMPP：ダッシュボード  ｜
    ｜http://localhost/phpmyadmin：phpMyAdmin          ｜
    ----------------------------------------------------
  */

  try{

    //事前準備
    $dbName = DBNAME_LIST[0];//接続するDB名をここで定義
    $DBC = new DB_Class();//新しいDBクラスを定義
    db_Init($DBC,DB_HOST,DB_USER,DB_PASSWORD,DB_PORT,$dbName);//DB情報を変更
    $NewPDO = pdo_Make($DBC);//DBに接続する為のPDOを生成
    $tableName = TABLENAME_LIST_ST[1];//attendanceinfo。接続するテーブル名をここで定義。これをやっておかないと、以下の!isset($_REQUEST['dbQuery'])にいれると２回目以降に無定義状態になるらしい。

    $y = date('Y'); $m = date('n');//現在の年月を取得
    $dataList = [
      'tn' => $tableName,
      'searchOption' => array('部分的AND条件','部分的OR条件','全体的AND条件','全体的AND条件','全体的AND条件','全体AND条件'),
      'column' => array('accessType','accessType','department','year','month','mgrOK'),
      'setValue' => array('ST', 'MGR',$_SESSION['DM'],$y,$m,'OK'),
      'searchType' => array('である','である','である','である','である','である')
    ];

    //準備（MGR以下の役職である社員の中から、承認休情報が確定している（mgrOKが'OK'になっている）社員の承認休情報を呼び出す）
    $showColumn = array("name",'year','month','accessType','department'); $showDayColumn = array();
    $last_day = date('j', mktime(0, 0, 0, $m + 1, 0, $y));// 月末日を取得
    for($i = 1; $i < $last_day + 1; $i++) {$showDayColumn[] = "day".$i; }
    $outputColumn = array_merge( $showColumn, $showDayColumn); //var_dump($outputColumn);
    $row = DB_PROCESS($dataList, $dbName, $tableName, $NewPDO, 'SEARCH_MODE', '', $outputColumn);
    //var_dump($row);

    $tdInfo = array();
    for($i = 0; $i < $last_day; $i++) {
      $tdInfo[$i] = "";//初期化
      $num = 0;
      for($j = 0; $j < count($row); $j++){
        $k = $i + 1; $column = 'day'.$k;
        if($row[$j][$column] == '出') {
          if($num > 0) { $tdInfo[$i] .= ',　'; }
          $tdInfo[$i] = $tdInfo[$i].$row[$j]['name'].'('.$row[$j]['department'].'・'.$row[$j]['accessType'].')';
          $num++;
        }
      }
    }

    //$y:対象年　$m:対象月　$num_X:列数　$num_Y:行数　$thInfo:見出し情報　$tdInfo:要素情報　$dStyle:divタグのスタイル　$tStyle:tableタグのスタイル　$showType:表示するタイプ
    $thInfo = ['日付','曜日','出勤者名'];
    $dStyle = ['id' => 'TEST', 'class' => 'scrX scrY cent', 'style' => 'height:500px;'];
    $tStyle = ['id' => 'TEST2', 'class' => 'table-striped table-dark', 'style' => 'width:100%; min-width:200px; max-width:100%; align:center;'];

    //勤怠情報を取得
    $tableName = TABLENAME_LIST_ST[2];//attendance_line。接続するテーブル名をここで定義。これをやっておかないと、以下の!isset($_REQUEST['dbQuery'])にいれると２回目以降に無定義状態になるらしい。
    $temp = "";
    if($m < 10) { $temp = $m.'/0'.$m; }else { $temp = $m.'/'.$m; }
    $dataList2 = ['tn' => $tableName,'searchOption' => array('全体的AND条件'),'column' => array('start'),'setValue' => array('2020/08'),'searchType' => array('で始まる')];

    $rowRow = DB_PROCESS($dataList2, $dbName, $tableName, $NewPDO, 'SEARCH_MODE', '', array('name','start','finish'));
    $dStyle2 = ['id' => 'TEST', 'class' => 'scrX scrY cent', 'style' => 'height:200px;'];
    //var_dump($rowRow);

    //$rowRowの中で閲覧できるものを制限
    $rowRow2 = array();
    for($k = 0; $k < count($rowRow); $k++){
      $TN2 = TABLENAME_LIST_ST[0];//employeeInfo
      $dataList3 = ['tn' => $TN2, 'searchOption' => array('全体的AND条件'), 'column' => array('name'), 'setValue' => array($rowRow[$k]['name']),'searchType' => array('である')];
      //var_dump($dataList3);
      $tempRow = DB_PROCESS($dataList3, $dbName, $TN2, $NewPDO, 'SEARCH_MODE', '',array('name','accessType','department'));
      //var_dump($tempRow);
      //var_dump($_SESSION['DM']);

      if($tempRow[0]['department'] == $_SESSION['DM'] && $tempRow[0]['accessType'] != 'EX'){
        //$rowRow[$k]['accessType'] = $tempRow[0]['accessType']; $rowRow[$k]['department'] = $tempRow[0]['department'];
        $rowRow2[] = $rowRow[$k];
      }
    }

    //var_dump($rowRow2);


  }catch(PDOException $e){
    header('Content-Type: text/plain; charset=UTF-8', true, 500);
    exit($e->getMessage()); //エラーの内容を吐き出す
  }
?>

<script>
//page topボタン
$(function(){

  //事前準備
  var userAgent = window.navigator.userAgent.toLowerCase();//使用しているブラウザを調べる
  var interF = "";
  if(userAgent.indexOf('msie') != -1 || userAgent.indexOf('trident') != -1) { interF = "IE"; }
  else if(userAgent.indexOf('edge') != -1) { interF = "Edge"; }
  else if(userAgent.indexOf('chrome') != -1) { interF = "Chrome"; }
  else if(userAgent.indexOf('safari') != -1) { interF = "Safari"; }
  else if(userAgent.indexOf('firefox') != -1) { interF = "firefox"; }
  else if(userAgent.indexOf('opera') != -1) { interF = "opera"; }
  var y = 0;
  var targetElement = document.getElementById( "temp" ) ;
  var clientRect = targetElement.getBoundingClientRect() ;
  var max_y = clientRect.top ;// 画面内の位置
  var pagetop=$('#target');
  pagetop.hide();

  setInterval(function(){

    if(interF == "Safari"){
      var y = window.pageYOffset;
      //alert(y);
      if(y > 300) { pagetop.fadeIn(); }
      else { pagetop.fadeOut(); }
    }
    else{
      targetElement = document.getElementById( "temp" ) ;
      clientRect = targetElement.getBoundingClientRect() ;
      y = clientRect.top ;// 画面内の位置
      var py = max_y - clientRect.top ;// ページ内の位置
      //console.log(py);
      if(py > 300) { pagetop.fadeIn(); /*$('.footer' + '.fixed-bottom').css('display','');*/ }
      else { pagetop.fadeOut(); }
    }
  },1000);

  //$(".sidebar-dropdown3 > a").removeClass("active");
  //$(".sidebar-dropdown3 > a").next(".sidebar-submenu3").slideDown(200);

});
</script>

<body class="user-profile">



<div class="page-wrapper chiller-theme toggled">
  <a id="show-sidebar" class="btn btn-sm btn-dark" href="#"><i class="fas fa-bars"></i></a>

  <!--sidebar-->
  <?php sidebarMake("シフト閲覧（部）",$_SESSION); ?>

  <!-- sidebar-wrapper  -->
  <main class="page-content">
    <div class="container-fluid">
      <div class="wrapper">
        <div class="main-panel" id="main-panel">

          <!-- End Navbar -->
          <div class="panel-header panel-header-sm"></div>

          <div class="content">
            <div class="row">

              <div class="col-md-12">
                <div class="card">
                  <div class="card-header"><h5 class="title">シフト閲覧（部）</h5></div>
                  <div class="card-body">
                    <div><?php shiftTableMakeShow($y, $m, $thInfo, $tdInfo, $dStyle, $tStyle); ?></div>
                    <br /><br /><br />
                    <div><?php EmployeeLineAttendanceShow($y, $m, EMPLOYEE_LINEATTENDANCE2_INFO_ASSOARRAY, $rowRow2, $dStyle2, $tStyle,'部内'); ?></div>
                  </div>
                </div>
              </div>

            </div>
          </div>

          <div id="temp" style="height:20vh;"></div>
          <footer class="footer fixed-bottom" id = "target" style="display:none;">
            <div class=" container-fluid ">
              <nav style="float:right;">
                <a href="#main-panel" style="float:right;"><i class="now-ui-icons arrows-1_minimal-up btn btn-orangeVer rounded-pill"></i></a>
              </nav>
            </div>
          </footer>

        </div>
      </div>

    </div>
  </main>
  <!-- page-content" -->
</div>
<!-- page-wrapper -->


  <!--   Core JS Files   -->
  <script src="../../BOOTSTRAP/JS/jquery.min.js"></script>
  <script src="../../BOOTSTRAP/JS/popper.min.js"></script>
  <script src="../../BOOTSTRAP/JS/bootstrap.min.js"></script>
  <script src="../../BOOTSTRAP/JS/bootstrap-notify.js"></script>
  <script src="../../BOOTSTRAP/JS/sidebar.js"></script>
  <script src="../../BOOTSTRAP/JS/clndr.js"></script>
  <script type="text/javascript">
    $('#clndr').clndr();
  </script>

</body>
</html>
