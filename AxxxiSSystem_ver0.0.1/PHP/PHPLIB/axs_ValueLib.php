<?php



//define(定数名,値)　→　要するに　” const 定数名 = 値 ”　のような感じ
define('BR_WIN',"<br>\n");//サーバーのOSがWindowsの時の改行コマンド（ネット上に色々あるけど、ブラウザ上ではこれしか動かない・・・）

///////////////////////////////////////////////////データベース情報（各自更新して使ってください）///////////////////////////////////////////////////


//主に用いるデータベースの情報を格納する変数(初期データベース情報)

//データベースが存在する場所へアクセスする際に必要な情報


/*村上さんローカル環境
define('DB_HOST','127.0.0.1');  //サーバのアドレス（ローカルの場合は127.0.0.1）
define('DB_USER','root');       //スタートページ(User)
define('DB_PASSWORD','');       //スタートページ(PassWord)
define('DB_PORT','3306');       //PORT番号（MySQLの番号をここに入力しておく）
*/

// 梶ローカル環境
// define('DB_HOST','localhost');  //サーバのアドレス（ローカルの場合は127.0.0.1）
// define('DB_USER','root');       //スタートページ(User)
// define('DB_PASSWORD','root');       //スタートページ(PassWord)
// define('DB_PORT','8889');       //PORT番号（MySQLの番号をここに入力しておく）

//本サーバ

define('DB_HOST','mysql145.phy.lolipop.lan');  //サーバのアドレス（ローカルの場合は127.0.0.1）
define('DB_USER','LAA1164453');       //スタートページ(User)
define('DB_PASSWORD','axxxis0730');       //スタートページ(PassWord)
define('DB_PORT','3306');       //PORT番号（MySQLの番号をここに入力しておく）



//MYSQLコマンドの省略用
define('DB_ALL_SELECT','SELECT * FROM ');
define('DB_CLEATE','CREATE TABLE IF NOT EXISTS ');

//データベースの詳細情報（データベース名・テーブル名）
//define('DBNAME_LIST',['st_db','ex_db']);//データベース名一覧（ローカル）
define('DBNAME_LIST',['LAA1164453-net','LAA1164453-net']);//データベース名一覧（サーバ）
define('TABLENAME_LIST_ST',['employeeinfo','attendanceinfo','attendance_line']);//一般スタッフ以上用のテーブル名一覧
define('TABLENAME_LIST_EX',['employeeinfo']);//マネージャー以上用のテーブル名一覧

//各データベースへアクセスするために必要な情報
define('DB_INFO_ST',"mysql:host=".DB_HOST.";dbname=".DBNAME_LIST[0].";port=".DB_PORT.";charset=utf8");
define('DB_INFO_EX',"mysql:host=".DB_HOST.";dbname=".DBNAME_LIST[1].";port=".DB_PORT.";charset=utf8");

//データベース初期アクセス時に使用する変数

//村上さんローカル環境
$FIRST_DBH = new PDO('mysql:host='.DB_HOST, DB_USER, DB_PASSWORD);

// 梶ローカル環境
//$FIRST_DBH = new PDO('mysql:dbname=LAA1164453-net;host=localhost;charset=utf8','root', 'root');

//扱いたいデータベースの情報を格納する為のDB専用クラス定義（複数のデータベースを併用する場合はこれを使おう）
class DB_Class{
  public $host, $user, $pw,  $sqlport, $dbname, $dbinfo;

  //コンストラクタ（初期化。要するにクラスを生成したときの各変数の初期値をここで定義）
  function __construct()	{
		$this->host = '127.0.0.1';
    $this->user = 'root';
    $this->pw = 'root';
    $this->sqlport = '3306';
    $this->dbname = 'Hello_DB';
    $this->dbinfo = 'first_dbinfo';
  }
}

//データベースを作成する際に必要な情報を格納するクラス（複数のデータベースを併用する場合はこれを使おう）
class DBMake_Class{
  public $src, $user, $pw, $dbname;//$srcはデータベースが存在するURL、userはユーザ名、$pwはパスワード、$dbnameは作成するデータベースの名前
  function __construct()	{ $this->src = ''; $this->user = ''; $this->pw = ''; $this->dbname = ''; }//コンストラクタ（初期化。要するにクラスを生成したときの各変数の初期値をここで定義）
}

////////////////////////////////////////////////////////////データベース情報（終わり）/////////////////////////////////////////////////////////////


//プルダウンメニューの選択肢（変数名,選択肢一覧）
define('PULLDOWNLIST_ACCESSTYPE',['EX','MGR','ST','ITST']);//プルダウンメニューの選択肢一覧（アクセス権限・役職）
define('PULLDOWNLIST_DEPARTMENT',['ICT','ITM','PM','ITP','ITS','OFFICER']);//プルダウンメニューの選択肢一覧（所属部署）
define('PULLDOWNLIST_ATTENDANCE',['出','休']);//プルダウンメニューの選択肢一覧（出勤日・選択式定休日選択）

?>
