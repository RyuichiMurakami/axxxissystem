<?php
// ローカル環境
// $accessToken = 'njuBQ/w3rjakKptiSHYot678qYAI/uefuye5lE099ZINA79zim1ZkYQDGUJHBVw9mbKPTBLiJoNiaoW+nNzPL84kaKm9SR97HBs7F/28pm82EvF5iaVeYmsW/Vi7ObXurvUPz4Z4qNdAjzlXL8ouhgdB04t89/1O/w1cDnyilFU=';

// 本番環境
$accessToken = 'GsneqAfqaR6urlX1oIgeksgE9GJzSCVCOppVDWqtZGjAUUUn7ehe0HzN96BDbRgEM0Lw67KBb+cJX7lU0X4IpZJ+JUXbGtN44iWfrQGkFVw1Y+zTHID8Vlqse3CfEgt66FRWe59pNxFkT9aWdK80tQdB04t89/1O/w1cDnyilFU=';

//ユーザーからのメッセージ取得
$json_string = file_get_contents('php://input');
$json_object = json_decode($json_string);

//取得データ
$replyToken = $json_object->{"events"}[0]->{"replyToken"};        //返信用トークン
$message_type = $json_object->{"events"}[0]->{"message"}->{"type"};    //メッセージタイプ
$message_text = $json_object->{"events"}[0]->{"message"}->{"text"};    //メッセージ内容
$timestamp = $json_object->{"events"}[0]->{"timestamp"};      // タイムスタンプ

// ユーザーIDの取得
$user_id = $json_object->{"events"}[0]->{"source"}->{"userId"};

// TimeStampをUTC→JSTに変換
date_default_timezone_set('Asia/Tokyo');
// TimeStampをDate型に変換
$timenow = date("Y/m/d AH:i:s",$timestamp / 1000);

//メッセージタイプが「text」以外のときは何も返さず終了
if($message_type != "text") exit;

//mysqlに接続 (ローカル環境)
// $db = new PDO('mysql:dbname=LAA1164453-net;host=localhost;charset=utf8','root', 'root');
// $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
// $db->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);

//mysqlに接続 (本番環境)
$db = new PDO('mysql:dbname=LAA1164453-net;host=mysql145.phy.lolipop.lan;charset=utf8','LAA1164453', 'axxxis0730');
$db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);

//返信メッセージ
if(strpos($message_text,'業務開始') !== false){

  // データ数を調べる
  $sql = "SELECT * FROM attendance_line";
  $stmt = $db->query($sql);
  $stmt->execute();
  $count = $stmt->rowCount();

  // ユーザーIDの探索
  $sql = "SELECT * FROM user_line WHERE user_id = '" . $user_id . "'";
  $stmt = $db->query($sql);
  $stmt->execute();
  $register = $stmt->rowCount();

  // ユーザー名取得
  $name = NULL; //初期化
  foreach ($stmt as $row) {
    $name = $row['name'];
  }

  // ユーザーIDの探索
  $sql = "SELECT * FROM attendance_line WHERE user_id = '" . $user_id . "'";
  $stmt = $db->query($sql);

  // startとfinishとIDの値を取得
  $start = NULL; //初期化
  $finish = NULL; //初期化
  $id = NULL; //初期化
  foreach ($stmt as $row) {
    $start = $row['start'];
    $finish = $row['finish'];
    $id = $row['id'];
  }

  if($register == 0){
    // レスポンス
    $return_message_text = "まだ名前の登録が完了してないよ！ \n@に続けて苗字と名前を書いてね！ \n例「@山田 太郎」";
  }else if($start == NULL & $finish == NULL){
    // データの追加
    $sql = "INSERT INTO attendance_line (id , name , user_id , start) VALUES (:id , :name , :user_id , :start)";
    $stmt = $db->prepare($sql);
    $params = array(':id' => $count+1 ,':name' => $name , ':user_id' => $user_id , ':start' => $timenow);
    $stmt->execute($params);

    // レスポンス
    $return_message_text = "業務開始ですね！ \n今日も１日お仕事頑張ってください！！";
  }else if($start != NULL & $finish != NULL){
    // データの追加
    $sql = "INSERT INTO attendance_line (id , name , user_id , start) VALUES (:id , :name , :user_id , :start)";
    $stmt = $db->prepare($sql);
    $params = array(':id' => $count+1 ,':name' => $name , ':user_id' => $user_id , ':start' => $timenow);
    $stmt->execute($params);

    // レスポンス
    $return_message_text = "業務開始ですね！ \n今日も１日お仕事頑張ってください！！";
  }else{
    // レスポンス
    $return_message_text = "既に業務開始してるよ！";
  }

}else if(strpos($message_text,'業務終了') !== false){

  // ユーザーIDの探索
  $sql = "SELECT * FROM attendance_line WHERE user_id = '" . $user_id . "'";
  $stmt = $db->query($sql);

  // finishとIDの値を取得
  $finish = NULL; //初期化
  $id = NULL; //初期化
  foreach ($stmt as $row) {
    $finish = $row['finish'];
    $id = $row['id'];
  }

  // ユーザーIDの探索
  $sql = "SELECT * FROM user_line WHERE user_id = '" . $user_id . "'";
  $stmt = $db->query($sql);
  $stmt->execute();
  $register = $stmt->rowCount();

  if($register == 0){
    // レスポンス
    $return_message_text = "まだ名前の登録が完了してないよ！ \n@に続けて苗字と名前を書いてね！ \n例「@山田 太郎」";
  }else if($finish == NULL){

    // finishの追記
    $sql = "UPDATE attendance_line SET finish = :finish WHERE id = :id";
    $stmt = $db->prepare($sql);
    $params = array(':finish' => $timenow, ':id' => $id);
    $stmt->execute($params);

    // レスポンス
    $return_message_text = "業務終了ですね！ \nお仕事お疲れ様でした！！";
  }else{
    // レスポンス
    $return_message_text = "まだ業務開始をしてないよ！";
  }

}else if(strpos($message_text,'@') !== false){

  // @を削除
  $message_text = str_replace('@', '', $message_text);

  // データ数を調べる
  $sql = "SELECT * FROM user_line";
  $stmt = $db->query($sql);
  $stmt->execute();
  $count=$stmt->rowCount();

  // ユーザーの追加
  $sql = "INSERT INTO user_line (id , name , user_id) VALUES (:id , :name , :user_id)";
  $stmt = $db->prepare($sql);
  $params = array(':id' => $count+1 ,':name' => $message_text , ':user_id' => $user_id);
  $stmt->execute($params);

  // レスポンス
  $return_message_text = "名前の登録が完了したよ！ \nこのチャットの使い方は一番最初に説明してるから、わからなくなったら見返してね！";

}
else if(strpos($message_text,'お腹すいた') !== false){ $return_message_text = "いや働けよ・・・"; /*レスポンス*/ }
else if(strpos($message_text,'予言者ウルマーニ') !== false){ $return_message_text = "お、おいやめとけって・・・\nうしろっ、うしろに社長が・・・"; /*レスポンス*/ }
else if(strpos($message_text,'ツンデレ') !== false){ $return_message_text = "アッ、アンタなんて全然\nタイプじゃないんだからね！"; /*レスポンス*/ }
else if(strpos($message_text,'⑨') !== false){ $return_message_text = "アタイはバカじゃないもん！\nバーカバーカ！"; /*レスポンス*/ }
else if(strpos($message_text,'働きたくないよー') !== false){ $return_message_text = "そうかっ、それじゃ、今月の君の給料は無しだ♪\nうひょおおお経費が浮くぜいぃぃぃ！"; /*レスポンス*/ }

else{
  $return_message_text = "ん？";
}

//レスポンスフォーマット
$response_format_text = ["type" => $message_type , "text" => $return_message_text];

//ポストデータ
$post_data = ["replyToken" => $replyToken , "messages" => [$response_format_text]];

$ch = curl_init("https://api.line.me/v2/bot/message/reply");
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charser=UTF-8','Authorization: Bearer ' . $accessToken));
$result = curl_exec($ch);
curl_close($ch);

//データベース接続切断
$db = NULL;



// デバック用
// define("TESTFILE","./TEST.txt");
// $fh = fopen(TESTFILE, "w");
// fwrite($fh,$result);
// fclose($fh);

?>
