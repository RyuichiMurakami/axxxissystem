<?php

include "axs_ValueLib.php";     //変数ライブラリーを読み込み

/////////////////////////データベース関連/////////////////////////

////新しくテーブルを作成する際に用いる配列////

//社員情報
define('EMPLOYEE_INFO_TABLE',[
  'no INT(20)','AUTO_INCREMENT PRIMARY KEY',//NO
  'id','VARCHAR(50)',//ID
  'pw','VARCHAR(50)',//パスワード
  'accessType','VARCHAR(20)',//役職（アクセス権限）
  'department','VARCHAR(20)',//部署
  'name','VARCHAR(50)',//名前
  'nameRuby','VARCHAR(50)',//ルビ（フリガナ）
  'sex','VARCHAR(10)',//性別
  'age','INT(11)',//年齢
  'birthday','VARCHAR(20)',//生年月日
  'zipCode' ,'VARCHAR(50)',//郵便番号
  'address' ,'VARCHAR(200)',//住所
  'telNumberHome','VARCHAR(20)',//電話番号（自宅）
  'telNumberMobile','VARCHAR(20)',//電話番号（携帯）
  'telNumberEmergency','VARCHAR(20)',//電話番号（緊急連絡先）
  'email' , 'VARCHAR(200)',//Email
  'alongLine','VARCHAR(200)',//最寄駅
  'selfAppeal','VARCHAR(400)',//自己アピール
  'qualification','VARCHAR(200)',//資格／免許
  'registryDatetime','DATETIME',//登録日時
  'lastUpdate','DATETIME']//最終更新
);

define('EMPLOYEE_INFO_TABLE_KEY',['id','pw','accessType','department','name','nameRuby','sex','age','birthday','zipCode','address','telNumberHome',
                                  'telNumberMobile','telNumberEmergency','email','alongLine','selfAppeal','qualification']);
define('EMPLOYEE_INFO_TABLE_LABEL',['社員ID','パスワード','役職（アクセス権限）','所属部署','氏名','フリガナ','性別','年齢','生年月日','郵便番号','住所','電話番号（自宅）',
                                  '電話番号（携帯）','電話番号（緊急）','Eメール','自己アピール','最寄駅','資格／免許']);

//社員希望休情報
define('EMPLOYEE_ATTENDANCE_INFO_TABLE',[
  'no INT(20)','AUTO_INCREMENT PRIMARY KEY',//NO
  'id','VARCHAR(50)',//ID（社員ID）
  'name','VARCHAR(50)',//名前
  'nameRuby','VARCHAR(50)',//ルビ（フリガナ）
  'accessType','VARCHAR(20)',//役職（アクセス権限）
  'department','VARCHAR(20)',//部署
  'year','INT(11)',//年度
  'month','INT(11)',//月
  'day1','VARCHAR(10)',//1日
  'day2','VARCHAR(10)',//2日
  'day3','VARCHAR(10)',//3日
  'day4','VARCHAR(10)',//4日
  'day5','VARCHAR(10)',//5日
  'day6','VARCHAR(10)',//6日
  'day7','VARCHAR(10)',//7日
  'day8','VARCHAR(10)',//8日
  'day9','VARCHAR(10)',//9日
  'day10','VARCHAR(10)',//10日
  'day11','VARCHAR(10)',//11日
  'day12','VARCHAR(10)',//12日
  'day13','VARCHAR(10)',//13日
  'day14','VARCHAR(10)',//14日
  'day15','VARCHAR(10)',//15日
  'day16','VARCHAR(10)',//16日
  'day17','VARCHAR(10)',//17日
  'day18','VARCHAR(10)',//18日
  'day19','VARCHAR(10)',//19日
  'day20','VARCHAR(10)',//20日
  'day21','VARCHAR(10)',//21日
  'day22','VARCHAR(10)',//22日
  'day23','VARCHAR(10)',//23日
  'day24','VARCHAR(10)',//24日
  'day25','VARCHAR(10)',//25日
  'day26','VARCHAR(10)',//26日
  'day27','VARCHAR(10)',//27日
  'day28','VARCHAR(10)',//28日
  'day29','VARCHAR(10)',//29日
  'day30','VARCHAR(10)',//30日
  'day31','VARCHAR(10)',//31日
  'mgrOK','VARCHAR(10)',//MGR承認
  'registryDatetime','DATETIME',//登録日時
  'lastUpdate','DATETIME']//最終更新
);

//希望休情報
define('EMPLOYEE_ATTENDANCE_INFO_ASSOARRAY',[
  'no' => 'NO',
  'id' => 'ID',
  'name' => '名前',
  'nameRuby' => 'フリガナ',
  'accessType' => '役職（アクセス権限）',
  'department'=> '所属部署',
  'accessType' => '役職（アクセス権限）',
  'year'=> '対象年',
  'month'=> '対象月',
  'day1' => '1日',
  'day2' => '2日',
  'day3' => '3日',
  'day4' => '4日',
  'day5' => '5日',
  'day6' => '6日',
  'day7' => '7日',
  'day8' => '8日',
  'day9' => '9日',
  'day10' => '10日',
  'day11' => '11日',
  'day12' => '12日',
  'day13' => '13日',
  'day14' => '14日',
  'day15' => '15日',
  'day16' => '16日',
  'day17' => '17日',
  'day18' => '18日',
  'day19' => '19日',
  'day20' => '20日',
  'day21' => '21日',
  'day22' => '22日',
  'day23' => '23日',
  'day24' => '24日',
  'day25' => '25日',
  'day26' => '26日',
  'day27' => '27日',
  'day28' => '28日',
  'day29' => '29日',
  'day30' => '30日',
  'day31' => '31日',
  'mgrOK'=> 'MGR承認',
  'registryDatetime' => '登録日時',
  'lastUpdate' => '最終更新']
);

//社員勤怠情報
define('EMPLOYEE_LINEATTENDANCE_INFO_TABLE',[
  'no INT(20)','AUTO_INCREMENT PRIMARY KEY',//NO
  'name','VARCHAR(50)',//名前
  'user_id','VARCHAR(50)',//ユーザID
  'start','VARCHAR(50)',//出勤時刻
  'finish','VARCHAR(50)']//退勤時刻
);

//勤怠情報
define('EMPLOYEE_LINEATTENDANCE_INFO_ASSOARRAY',[
  'no' => 'NO',
  'name' => '名前',
  'user_id' => 'ユーザID',
  'start' => '出勤時刻',
  'finish' => '退勤時刻']
);

//勤怠情報（改良版）
define('EMPLOYEE_LINEATTENDANCE2_INFO_ASSOARRAY',[
  'name' => '名前',
  'start' => '出勤時刻',
  'finish' => '退勤時刻']
);

//勤怠情報（改良版）
define('EMPLOYEE_LINEATTENDANCE3_INFO_ASSOARRAY',[
  'name' => '名前',
  'start' => '出勤時刻',
  'finish' => '退勤時刻',
  'accessType' => '役職',
  'department' => '所属部署',]
);

//社員取得情報（LINE）
define('EMPLOYEE_USERLINE_INFO_TABLE',[
  'id INT(20)','AUTO_INCREMENT PRIMARY KEY',//NO
  'name','VARCHAR(50)',//名前
  'user_id','VARCHAR(50)',//ユーザID
  'accessType','VARCHAR(20)',//役職（アクセス権限）
  'department','VARCHAR(20)',//部署
]);

//取得情報（LINE）
define('EMPLOYEE_USERLINE_INFO_ASSOARRAY',[
  'no' => 'NO',
  'name' => '名前',
  'user_id' => 'ユーザID',
  'accessType' => '役職（アクセス権限）',
  'department'=> '所属部署']
);



///////////////////////////////////////////

define('DB_TABLELIST_ST',[EMPLOYEE_INFO_TABLE,EMPLOYEE_ATTENDANCE_INFO_TABLE,EMPLOYEE_LINEATTENDANCE_INFO_TABLE]);//一般スタッフ用のデータベース以下のテーブルの詳細情報一覧
define('DB_TABLELIST_EX',['AA','BB','CC']);//マネージャー以上用のデータベース以下のテーブルの詳細情報一覧
define('USEFUL_UNSET_KEY',['no','registryDatetime','lastUpdate']);//よく削除する連想配列のキー群
define('SPELL_TO_NUM',['','A','B','C','D','E','F','G','H','I','J','K','L','M','N',
                       'O','P','Q','R','S','T','U','V','W','X','Y','Z',26]);//英字のスペルと数字の相互変換に使用する配列（A ⇔ 1, B ⇔ 2,・・・,Z ⇔ 26　ちなみに最初の配列は左上の空文字、最後の配列は英文字のスペル数、つまり２６）
define('CSS_STYLE_SET',[
  'min50w' => 'min-width:50px;',
  'min150w' => 'min-width:150px;',
  'padding0' => 'padding:0;',
  'veral-mid' => 'vertical-align:middle;',
  'border1px' => 'border:1px solid #ced4da;',
]);


//カラム名一覧
define('TABLE_LIST_COLUMN_NAME',[
  'EMPLOYEE_ATTENDANCE_INFO_ASSOARRAY']
);

//データベースへ初回アクセスする際に自動生成される社員情報
define('INIT_EMPLOYEE_DATA',[
  'id' => 'EID',
  'pw' => 'EPW',
  'accessType' => 'EX',
]);

define('INIT_EMPLOYEE_DATA2',[
  'id' => 'EID2',
  'pw' => 'EPW2',
  'name' => '小三元',
  'accessType' => 'MGR',
]);

define('INIT_EMPLOYEE_DATA3',[
  'id' => '316410',
  'pw' => 'EPW3',
  'accessType' => 'ITST',
  'department' => 'ITP',
  'name' => '吉野五郎',
  'nameRuby' => 'ヨシノゴロウ',
  'sex' => '男',
  'age' => '28',
  'birthday' => '1991-09-23',
]);

define('INIT_EMPLOYEE_DATA4',[
  'id' => '281657',
  'pw' => 'YOKOYAMA',
  'accessType' => 'MGR',
  'department' => 'ICT',
  'name' => '横山',
  'nameRuby' => 'ヨコヤマ',
  'sex' => '男',
  'age' => '',
  'birthday' => '1991-09-23',
]);

define('INIT_EMPLOYEE_LINEATTENDANCE_DATA',['name' => '神撃のサララン', 'user_id' => 'tei43oTJKGyirtoK9JI09Ojpoij', 'start' => '2020/08/07 PM14:44:14', 'finish' => '2020/08/07 PM15:44:14',]);
define('INIT_EMPLOYEE_LINEATTENDANCE_DATA2',['name' => '吉永さつき', 'user_id' => 'tlyukiFNTITM350FJ9uj49gjrks', 'start' => '2020/08/09 PM15:12:28', 'finish' => '2020/08/07 PM18:49:54',]);

define('INIT_EMPLOYEE_USERLINE_DATA',['name' => '神撃のサララン', 'user_id' => 'tei43oTJKGyirtoK9JI09Ojpoij', 'accessType' => 'MGR','department'=> 'ITM',]);

/////////////////////////////////////////////////////////////////



?>
