$(".sidebar-dropdown > a").click(function() {
  $(".sidebar-submenu").slideUp(200);
  if (
    $(this)
      .parent()
      .hasClass("active")
  ) {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .parent()
      .removeClass("active");
  } else {
    $(".sidebar-dropdown").removeClass("active");
    $(this)
      .next(".sidebar-submenu")
      .slideDown(200);
    $(this)
      .parent()
      .addClass("active");
  }
});

$(".sidebar-dropdown1 > a").click(function() {
  $(".sidebar-submenu1").slideUp(200);
  if ( $(this).parent().hasClass("active") ) {
    $(".sidebar-dropdown1").removeClass("active");
    $(this).parent().removeClass("active");
  } else {
    $(".sidebar-dropdown1").removeClass("active");
    $(this).next(".sidebar-submenu1").slideDown(200);
    $(this).parent().addClass("active");
  }
});

$(".sidebar-dropdown2 > a").click(function() {
  $(".sidebar-submenu2").slideUp(200);
  if ( $(this).parent().hasClass("active") ) {
    $(".sidebar-dropdown2").removeClass("active");
    $(this).parent().removeClass("active");
  } else {
    $(".sidebar-dropdown2").removeClass("active");
    $(this).next(".sidebar-submenu2").slideDown(200);
    $(this).parent().addClass("active");
  }
});

$(".sidebar-dropdown3 > a").click(function() {
  $(".sidebar-submenu3").slideUp(200);
  if ( $(this).parent().hasClass("active") ) {
    $(".sidebar-dropdown3").removeClass("active");
    $(this).parent().removeClass("active");
  } else {
    $(".sidebar-dropdown3").removeClass("active");
    $(this).next(".sidebar-submenu3").slideDown(200);
    $(this).parent().addClass("active");
  }
});

$("#close-sidebar").click(function() {
  $(".page-wrapper").removeClass("toggled");
});
$("#show-sidebar").click(function() {
  $(".page-wrapper").addClass("toggled");
});
