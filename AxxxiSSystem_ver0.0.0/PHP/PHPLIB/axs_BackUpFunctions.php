<?php

//入力値を、DBに格納するためのデータ配列にして返す関数
function insertExe($request, $tableName, $dbhinfo){
  //ここでまず、今選択されているテーブルに登録されている全情報を取得して表示させる。
  unset($request['PHPSESSID']);//何か勝手に入ってるので削除（ある日突然入るようになった、いったい何が起こってるの？）
  $dbhinfo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);//クエリのバッファーを使うことを宣言・準備をしておく。
  $sql = "SELECT * FROM ".$tableName;//テーブル"testquery5"の全レコードを選択
  $stmt = $dbhinfo->query($sql);//システムコマンド、クエリバッファーをジェネレート！！　←いや違うでしょ・・・　（executeでSQLを実行）
  $stmt->execute();

  $asso_key = asso_ArrayValueReturn($request,"KEY","登録する");//連想配列に格納されているキー群を配列に格納
  $asso_data = asso_ArrayValueReturn($request,"DATA","登録する");//連想配列に格納されているデータ群を配列に格納(各項目の値を配列で格納)
  $sql2 = prepareSQL($tableName, $asso_key,"登録する" );//sql命令文を作成して格納
  $stmt2 = $dbhinfo->prepare($sql2);// 挿入する値は空のまま、SQL実行の準備をする

  $params = assoArrayMake_FromRequest($asso_key,$asso_data,"登録する");
  $stmt2->execute($params);// 挿入する値が入った連想配列　"$params"　をexecuteにセットしてSQLを実行
}

//入力値を、DBに格納するためのデータ配列にして返す関数
function updateExe($request, $tableName, $dbhinfo){
  //ここでまず、今選択されているテーブルに登録されている全情報を取得して表示させる。
  $dbhinfo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);//クエリのバッファーを使うことを宣言・準備をしておく。
  $sql = "SELECT * FROM ".$tableName;//テーブル"testquery5"の全レコードを選択
  $stmt = $dbhinfo->query($sql);//システムコマンド、クエリバッファーをジェネレート！！　←いや違うでしょ・・・　（executeでSQLを実行）
  $stmt->execute();

  $asso_key = asso_ArrayValueReturn($request,"KEY","更新する");//連想配列に格納されているキー群を配列に格納
  $asso_data = asso_ArrayValueReturn($request,"DATA","更新する");//連想配列に格納されているデータ群を配列に格納(各項目の値を配列で格納)
  $params = updateAssoarrayMake($asso_key, $asso_data);//値を更新する際に用いる連想配列を予め作成しておく
  $assoKey = fromAssoarray_toArray($params, "KEY");//連想配列 "$params" のキーを配列に変換し、 "$assoKey" に格納
  $sql2 = prepareSQL($tableName, $assoKey, "更新する" );//sql命令文を作成して格納
  $stmt2 = $dbhinfo->prepare($sql2);// 挿入する値は空のまま、SQL実行の準備をする。プレースホルダ―（値を入れるための単なる空箱）を用意し、値が空のままのSQL文をセット
  $stmt2->execute($params);// 挿入する値が入った連想配列　"$params"　をexecuteにセットしてSQLを実行
}

//入力値を、DBに格納するためのデータ配列にして返す関数
function updateExe2($request, $tableName, $dbhinfo){
  $conditions = $request['conditions0'];//条件（no or id）
  $condValue = $request['condvalue0'];//条件の値
  $selectColumn = $request['column0'];//選択されたカラム名（KEY）
  $setColumnValue = $request['value0'];//更新後のカラムの値

  //ここでまず、今選択されているテーブルに登録されている全情報を取得して表示させる。
  $dbhinfo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);//クエリのバッファーを使うことを宣言・準備をしておく。

  date_default_timezone_set('Asia/Tokyo');//日時を日本標準時刻に変更
  $now = date("Y/m/d H:i:s");//現在日時を取得（最終更新に入る）
  $sql2 = "UPDATE $tableName SET ".$selectColumn." = :".$selectColumn.", lastUpdate = :lastUpdate WHERE ".$conditions." = :".$conditions;//sql命令文を作成して格納
  $stmt2 = $dbhinfo->prepare($sql2);// 挿入する値は空のまま、SQL実行の準備をする。プレースホルダ―（値を入れるための単なる空箱）を用意し、値が空のままのSQL文をセット
  $params = array(":".$selectColumn => $setColumnValue,":lastUpdate" => $now,":".$conditions => $condValue );//値を更新する際に用いる連想配列を作成
  $stmt2->execute($params);// 挿入する値が入った連想配列　"$params"　をexecuteにセットしてSQLを実行
}

//入力値を、DBに格納するためのデータ配列にして返す関数
function profileUpdateExe($request, $tableName, $dbhinfo){
  //ここでまず、今選択されているテーブルに登録されている全情報を取得して表示させる。
  $dbhinfo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);//クエリのバッファーを使うことを宣言・準備をしておく。

  $asso_key = asso_ArrayValueReturn($request,"KEY","プロフィールを更新する");//連想配列に格納されているキー群を配列に格納
  $asso_data = asso_ArrayValueReturn($request,"DATA","プロフィールを更新する");//連想配列に格納されているデータ群を配列に格納(各項目の値を配列で格納)

  for($i = 0; $i < count($asso_key); $i++){ $params[$asso_key[$i]] = $asso_data[$i]; }//値を更新する際に用いる連想配列を予め作成しておく
  $assoKey = fromAssoarray_toArray($params, "KEY");//連想配列 "$params" のキーを配列に変換し、 "$assoKey" に格納
  $sql2 = prepareSQL($tableName, $assoKey, "プロフィールを更新する" );//sql命令文を作成して格納
  $stmt2 = $dbhinfo->prepare($sql2);// 挿入する値は空のまま、SQL実行の準備をする。プレースホルダ―（値を入れるための単なる空箱）を用意し、値が空のままのSQL文をセット
  $stmt2->execute($params);// 挿入する値が入った連想配列　"$params"　をexecuteにセットしてSQLを実行
}

function searchExe($request, $dbName, $tableName, $dbhInfo, $selectColumn){
  //事前準備
  $dbhInfo->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);//クエリのバッファーを使うことを宣言・準備をしておく。

  $needColumn = "";
  if($selectColumn != "") {
    $needColumn.= " ".$selectColumn[0];
    for($i = 1; $i < count($selectColumn); $i++){ $needColumn.= ", ".$selectColumn[$i]; }
  }
  else{$needColumn = " *"; }

  $sql2 = "SELECT".$needColumn." FROM ".$tableName." WHERE ";//初めの条件文をここで指定

  for($i = 0; $i < count($request['searchOption']); $i++){
    $st = $request['searchType'][$i];//完全一致か部分一致かをここに格納

    if(count($request['searchOption']) == 1){//そもそも検索条件が一つしかない場合
      if($st == "である"){ $sql2 .= $request['column'][$i]." = "."'".$request['setValue'][$i]."'"; }//テーブル"testquery5"の全レコードを選択
      elseif($st == "で始まる"){$sql2 .= $request['column'][$i]." like "."'".$request['setValue'][$i]."%'";}
      elseif($st == "で終わる"){$sql2 .= $request['column'][$i]." like "."'%".$request['setValue'][$i]."'";}
      elseif($st == "を含む"){$sql2 .= $request['column'][$i]." like "."'%".$request['setValue'][$i]."%'";}
    }

    else{
      if($request['searchOption'][$i] == "全体的AND条件" || $request['searchOption'][$i] == "全体的OR条件") { $type = "ALL"; }
      else {$type = "PART";}

      if($request['searchOption'][$i] == "全体的AND条件" || $request['searchOption'][$i] == "部分的AND条件") { $searchType = "AND"; }
      if($request['searchOption'][$i] == "全体的OR条件" || $request['searchOption'][$i] == "部分的OR条件") { $searchType = "OR"; }

      if($i == 0){
        if($request['searchOption'][$i] == "部分的AND条件" || $request['searchOption'][$i] == "部分的OR条件"){ $sql2 .= "( "; }
        else if($request['searchOption'][$i + 1] == "部分的AND条件" || $request['searchOption'][$i + 1] == "部分的OR条件"){ $sql2 .= "( "; }

        if($st == "である"){ $sql2 .= $request['column'][$i]." = "."'".$request['setValue'][$i]."'"; }//テーブル"testquery5"の全レコードを選択
        elseif($st == "で始まる"){$sql2 .= $request['column'][$i]." like "."'".$request['setValue'][$i]."%'";}
        elseif($st == "で終わる"){$sql2 .= $request['column'][$i]." like "."'%".$request['setValue'][$i]."'";}
        elseif($st == "を含む"){$sql2 .= $request['column'][$i]." like "."'%".$request['setValue'][$i]."%'";}
      }

      else if($i == count($request['searchOption']) - 1){

        if($request['searchOption'][$i-1] == "全体的AND条件" || $request['searchOption'][$i-1] == "全体的OR条件") { $type_pre = "ALL"; }
        else {$type_pre = "PART";}

        if($type_pre == "PART" && $type == "ALL"){
          if(mb_substr($sql2, -1) != ")" ) { $sql2 .= " ) "; }
          $sql2 .= $searchType." ";
        }
        else {$sql2 .= " ".$searchType." ";}

        if($st == "である"){ $sql2 .= $request['column'][$i]." = "."'".$request['setValue'][$i]."'"; }//テーブル"testquery5"の全レコードを選択
        elseif($st == "で始まる"){$sql2 .= $request['column'][$i]." like "."'".$request['setValue'][$i]."%'";}
        elseif($st == "で終わる"){$sql2 .= $request['column'][$i]." like "."'%".$request['setValue'][$i]."'";}
        elseif($st == "を含む"){$sql2 .= $request['column'][$i]." like "."'%".$request['setValue'][$i]."%'";}

        if($type_pre == "ALL" && $type == "PART"){ if(mb_substr($sql2, -1) != ")" ){ $sql2 .= " )"; } }
        if($type_pre == "PART" && $type == "PART"){ if(mb_substr($sql2, -1) != ")" ){ $sql2 .= " )"; } }
      }

      else{

        if($request['searchOption'][$i-1] == "全体的AND条件" || $request['searchOption'][$i-1] == "全体的OR条件") { $type_pre = "ALL"; }
        else {$type_pre = "PART";}

        if($request['searchOption'][$i+1] == "全体的AND条件" || $request['searchOption'][$i+1] == "全体的OR条件") { $type_post = "ALL"; }
        else {$type_post = "PART";}

        if($type_pre == "PART" && $type == "ALL" && $type_post == "ALL"){ if(mb_substr($sql2, -1) != ")" ){ $sql2 .= " )"; }  }
        if($type_pre == "PART" && $type == "ALL" && $type_post == "PART"){ if(mb_substr($sql2, -1) != ")" ){ $sql2 .= " )"; } }

        $sql2.= " ".$searchType." ";
        if($type_pre == "ALL" && $type == "ALL" && $type_post == "PART"){ if(mb_substr($sql2, -2) != "( " ) { $sql2.="( "; } }
        if($type_pre == "PART" && $type == "ALL" && $type_post == "PART"){ if(mb_substr($sql2, -2) != "( " ) { $sql2.="( "; } }

        if($st == "である"){ $sql2 .= $request['column'][$i]." = "."'".$request['setValue'][$i]."'"; }//テーブル"testquery5"の全レコードを選択
        elseif($st == "で始まる"){$sql2 .= $request['column'][$i]." like "."'".$request['setValue'][$i]."%'";}
        elseif($st == "で終わる"){$sql2 .= $request['column'][$i]." like "."'%".$request['setValue'][$i]."'";}
        elseif($st == "を含む"){$sql2 .= $request['column'][$i]." like "."'%".$request['setValue'][$i]."%'";}

        if($type_pre == "ALL" && $type == "PART" && $type_post == "ALL"){ if(mb_substr($sql2, -1) != ")" ){ $sql2 .= " )"; }  }
        if($type_pre == "PART" && $type == "PART" && $type_post == "ALL"){ if(mb_substr($sql2, -1) != ")" ){ $sql2 .= " )"; }  }
      }
    }
  }

  $stmt2 = $dbhInfo->prepare($sql2);// 挿入する値は空のまま、SQL実行の準備をする
  $row = array();
  get_NameList($row,$dbhInfo,"COLUMNLIST",$dbName, $tableName);//カラム名取得
  $rowRow = array();
  $statement = $dbhInfo->query($sql2);//全ての情報を保存
  $rowRow = $statement->fetchAll(PDO::FETCH_ASSOC);//データベースを配列情報に変換して、入れる。PDO::FETCH_ASSOC・・・フィールド名で添字を付けた配列を返す
  return $rowRow;
}

function deleteExe($request,$tableName,$dbhInfo){
  $sql ="DELETE FROM $tableName WHERE no = :no";// DELETE文を変数に格納
  $stmt = $dbhInfo->prepare($sql);// 削除するレコードのNOは空のまま、SQL実行の準備をする
  $params = array(':no' => $request['del_no']);// 削除するレコードのNOを、連想配列 "$params" に格納する( 実質的には・・・ $params[':no'] = $_REQUEST['del_no']; )
  $stmt->execute($params);// 削除するレコードのNOが入った変数をexecuteにセットしてSQLを実行
}

function deleteExe2($request,$tableName,$dbhInfo){
  $conditions = $request['condition'];
  $sql ="DELETE FROM $tableName WHERE ".$conditions." = :".$conditions;// DELETE文を変数に格納
  $stmt = $dbhInfo->prepare($sql);// 削除するレコードのNOは空のまま、SQL実行の準備をする
  $params = array(":".$conditions => $request['del_no']);
  $stmt->execute($params);// 削除するレコードのNOが入った変数をexecuteにセットしてSQLを実行
}

 ?>
